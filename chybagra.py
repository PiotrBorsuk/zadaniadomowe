import random

wartosci_komorkowe = ['', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',' ']
czy_x_wygral = False
czy_o_wygral = False
wygrana_x = ['X','X','X']
wygrana_o = ['O','O','O']
liczba_ruchow = 0
p = 0
while __name__ == '__main__':
    if czy_x_wygral == False and czy_o_wygral == False:
        miejsce_krzyzyka = int(input(f'''W którym miejscu chcesz postawić krzyżyk? Aktualna tablica:
    {wartosci_komorkowe[1]}|{wartosci_komorkowe[2]}|{wartosci_komorkowe[3]}
    {wartosci_komorkowe[4]}|{wartosci_komorkowe[5]}|{wartosci_komorkowe[6]}
    {wartosci_komorkowe[7]}|{wartosci_komorkowe[8]}|{wartosci_komorkowe[9]}\n'''))
        print()
        if miejsce_krzyzyka > 9 or miejsce_krzyzyka == 0:
            print('Nie ma takiego miejsca na planszy, spróbuj cyfry od 1 do 9')
            continue
        if wartosci_komorkowe[miejsce_krzyzyka] == ' ':
            wartosci_komorkowe[miejsce_krzyzyka] = 'X'
            liczba_ruchow += 1
        else:
            print('To pole jest już zajęte, spróbuj inne')
            continue
        first_row = wartosci_komorkowe[1:4]
        second_row = wartosci_komorkowe[4:7]
        third_row = wartosci_komorkowe[7:10]
        vertical_first_row = wartosci_komorkowe[1:8:3]
        vertical_second_row = wartosci_komorkowe[2:9:3]
        vertical_third_row = wartosci_komorkowe[3:10:3]
        skos_prawy = wartosci_komorkowe[1:10:4]
        skos_lewy = wartosci_komorkowe[3:8:2]
        if wygrana_x == first_row or wygrana_x == second_row or wygrana_x == third_row or wygrana_x == vertical_first_row \
                or wygrana_x==vertical_second_row or wygrana_x==vertical_third_row or wygrana_x==skos_lewy or \
                wygrana_x==skos_prawy:
            czy_x_wygral = True
            continue

        while p == 0:
            a = random.randint(1, 9)
            if liczba_ruchow >= 5:
                p +=1
            elif liczba_ruchow < 5:
                if wartosci_komorkowe[a] != ' ':
                    continue
                else:
                    wartosci_komorkowe[a] = 'O'
                    break
        first_row = wartosci_komorkowe[1:4]
        second_row = wartosci_komorkowe[4:7]
        third_row = wartosci_komorkowe[7:10]
        vertical_first_row = wartosci_komorkowe[1:8:3]
        vertical_second_row = wartosci_komorkowe[2:9:3]
        vertical_third_row = wartosci_komorkowe[3:10:3]
        skos_prawy = wartosci_komorkowe[1:10:4]
        skos_lewy = wartosci_komorkowe[3:8:2]
        if czy_x_wygral == False:
            if wygrana_o == first_row or wygrana_o == second_row or wygrana_o == third_row or wygrana_o == vertical_first_row \
                    or wygrana_o == vertical_second_row or wygrana_o == vertical_third_row or wygrana_o == skos_lewy or \
                    wygrana_o == skos_prawy:
                czy_o_wygral = True
                continue
            else:
                continue
        else:
            continue


    if not ' ' in wartosci_komorkowe and czy_x_wygral==False:
        print('remis')
        break
    if czy_x_wygral == True:
        print('Gracz X wygrał grę.')
        print(wartosci_komorkowe[1:4])
        print(wartosci_komorkowe[4:7])
        print(wartosci_komorkowe[7:10])
        break
    if czy_o_wygral == True:
        print('Gracz O wygrał grę.')
        print(wartosci_komorkowe[1:4])
        print(wartosci_komorkowe[4:7])
        print(wartosci_komorkowe[7:10])
        break
