import random

wartosci_komorkowe = [' ',' ',' ',' ',' ',' ',' ',' ',' ']
czy_x_wygral = False
czy_o_wygral = False
while 1 >0:
  if czy_x_wygral == False and czy_o_wygral == False:
    miejsce_krzyzyka = int(input(f'''W którym miejscu chcesz postawić krzyżyk? Aktualna tablica:
    {wartosci_komorkowe[0]}|{wartosci_komorkowe[1]}|{wartosci_komorkowe[2]}
    {wartosci_komorkowe[3]}|{wartosci_komorkowe[4]}|{wartosci_komorkowe[5]}
    {wartosci_komorkowe[6]}|{wartosci_komorkowe[7]}|{wartosci_komorkowe[8]}\n'''))
    print()
    if miejsce_krzyzyka > 8:
      print('Nie ma takiego miejsca na planszy, spróbuj cyfry od 0 do 8')
      continue
    if wartosci_komorkowe[miejsce_krzyzyka] == ' ':
        wartosci_komorkowe[miejsce_krzyzyka] = 'X'
    else:
        print('To pole jest już zajęte, spróbuj inne')
        continue
    
    for x in range(0,6):
      if (wartosci_komorkowe[x] == wartosci_komorkowe[x+1] == wartosci_komorkowe[x+2]=='X')\
              or (wartosci_komorkowe[0]==wartosci_komorkowe[4]==wartosci_komorkowe[8]=='X')\
              or (wartosci_komorkowe[2]==wartosci_komorkowe[4]==wartosci_komorkowe[6]=='X')\
              or (wartosci_komorkowe[0]==wartosci_komorkowe[3]==wartosci_komorkowe[6]=='X')\
              or (wartosci_komorkowe[1]==wartosci_komorkowe[4]==wartosci_komorkowe[7]=='X')\
              or (wartosci_komorkowe[3]==wartosci_komorkowe[5]==wartosci_komorkowe[8]=='X'):
          czy_x_wygral = True
          break
      else:
          x += 1
    while 1>0:
      a = random.randint(0,8)
      if wartosci_komorkowe[a] != ' ':
        continue
      else:
        wartosci_komorkowe[a] = 'O'
        break
    if czy_x_wygral == False:
      for x in range(0,6):
          if (wartosci_komorkowe[x] == wartosci_komorkowe[x + 1] == wartosci_komorkowe[x + 2] == 'O') \
                  or (wartosci_komorkowe[0] == wartosci_komorkowe[4] == wartosci_komorkowe[8] == 'O') \
                  or (wartosci_komorkowe[2] == wartosci_komorkowe[4] == wartosci_komorkowe[6] == 'O') \
                  or (wartosci_komorkowe[0] == wartosci_komorkowe[3] == wartosci_komorkowe[6] == 'O') \
                  or (wartosci_komorkowe[1] == wartosci_komorkowe[4] == wartosci_komorkowe[7] == 'O') \
                  or (wartosci_komorkowe[3] == wartosci_komorkowe[5] == wartosci_komorkowe[8] == 'O'):
              czy_o_wygral = True
              break
      else:
        x += 1
    else:
      continue
  elif czy_x_wygral == True:
    print('Gracz X wygrał grę.')
    print(wartosci_komorkowe[0:3])
    print(wartosci_komorkowe[3:6])
    print(wartosci_komorkowe[6:9])
    break
  elif czy_o_wygral == True:
    print('Gracz O wygrał grę.')
    print(wartosci_komorkowe[0:3])
    print(wartosci_komorkowe[3:6])
    print(wartosci_komorkowe[6:9])
    break
  else:
    print('Remis')
    break
